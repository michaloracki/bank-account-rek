from django.contrib import admin
from bank_account.models import *

admin.site.register(AccountType)
admin.site.register(Account)
admin.site.register(AccountStatement)
admin.site.register(TransactionType)
admin.site.register(Transaction)


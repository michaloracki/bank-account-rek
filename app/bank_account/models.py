from django.db import models
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField
from bank_account.managers import IncomeManager, LossManager, CreditManager, DebitManager

class AccountType(models.Model):
    SAVINGS_ACCOUNT = 'savings'
    CURRENT_ACCOUNT = 'current'
    SALARY_ACCOUNT = 'salary'
    OTHER = 'other'
    ACCOUNT_CHOICES = [
        (SAVINGS_ACCOUNT, 'Savings account'),
        (CURRENT_ACCOUNT, 'Current account'),
        (SALARY_ACCOUNT, 'Salary account'),
        (OTHER, 'Other')
    ]
    name = models.CharField(verbose_name=_('name'), max_length=8, choices=ACCOUNT_CHOICES)

    def __str__(self) -> str:
        return self.name


class Account(models.Model):
    account_no = models.CharField(verbose_name=_('account number'), max_length=32, primary_key=True)
    account_type = models.ForeignKey(AccountType, on_delete=models.PROTECT)
    first_name = models.CharField(verbose_name=_('first name'), max_length=100)
    second_name = models.CharField(verbose_name=_('second name'), max_length=100, null=True, blank=True)
    last_name = models.CharField(verbose_name=_('last name'), max_length=100)
    country = CountryField()
    place_of_birth = models.CharField(verbose_name=_('place of birth'), max_length=200)
    date_of_birth = models.DateField(verbose_name=_('date of birth'))
    personal_id_number = models.CharField(verbose_name=_('personal id number'), max_length=32, null=True, blank=True)
    city = models.CharField(verbose_name=_('city'), max_length=100)
    address = models.CharField(verbose_name=_('address'), max_length=100)
    zip_code = models.CharField(verbose_name=_('address'), max_length=10)
    phone_number = models.CharField(verbose_name=_('phone number'), max_length=15)
    email = models.EmailField()

    def __str__(self) -> str:
        return self.account_no


class TransactionType(models.Model):
    ADJ_CREDIT = 'A'
    ADJ_DEBIT = 'a'
    DEPOSIT = 'D'
    BANK_FEE = 'F'
    WITHDRAWAL = 'W'
    ATM_WITHDRAWAL = 'w'
    TRANSACTION_CHOICES = [
        (ADJ_CREDIT, 'Adjustment Credit'),
        (ADJ_DEBIT, 'Adjustment Debit'),
        (DEPOSIT, 'Deposit'),
        (BANK_FEE, 'Bank Fee'),
        (WITHDRAWAL, 'Withdrawal'),
        (ATM_WITHDRAWAL, 'ATM Withdrawal')
    ]

    transaction_type_name = models.CharField(verbose_name=_('transaction type name'), max_length=1, choices=TRANSACTION_CHOICES)

    def __str__(self) -> str:
        choices_dict = dict(TransactionType.TRANSACTION_CHOICES)
        return choices_dict.get(self.transaction_type_name)


class Transaction(models.Model):
    account_no = models.ForeignKey(Account, on_delete=models.PROTECT)
    transaction_type = models.ForeignKey(TransactionType, on_delete=models.PROTECT)
    date = models.DateTimeField(verbose_name=('transaction date'))
    ammount = models.DecimalField(verbose_name=_('ammount'), max_digits=20, decimal_places=4)

    objects = models.Manager()
    income = IncomeManager(TransactionType)
    loss = LossManager(TransactionType)
    credit = CreditManager(TransactionType)
    debit = DebitManager(TransactionType)

    def __str__(self) -> str:
        return f'{self.account_no} | {self.transaction_type} | {self.ammount}'


class AccountStatement(models.Model):
    account_no = models.ForeignKey(Account, on_delete=models.PROTECT)
    date = models.DateTimeField(verbose_name=('transaction date'))
    closing_balance = models.DecimalField(verbose_name=_('closing balance'), max_digits=20, decimal_places=4)
    total_credit = models.DecimalField(verbose_name=_('total credit'), max_digits=20, decimal_places=4, default=0)
    total_debit = models.DecimalField(verbose_name=_('total debit'), max_digits=20, decimal_places=4, default=0)


    def __str__(self) -> str:
        return f'Account statement no: {self.id}'
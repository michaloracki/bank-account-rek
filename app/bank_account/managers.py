from django.db import models
from django.db.models import Q


class IncomeManager(models.Manager):
    def __init__(self, type_model) -> None:
        self.type_model = type_model
        super().__init__()

    def get_queryset(self):
        q = self.type_model.objects.filter(Q(transaction_type_name=self.type_model.ADJ_CREDIT) | 
                                             Q(transaction_type_name=self.type_model.DEPOSIT))
        return super().get_queryset().filter(transaction_type__in=q)


class LossManager(models.Manager):
    def __init__(self, type_model) -> None:
        self.type_model = type_model
        super().__init__()

    def get_queryset(self):
        q = self.type_model.objects.filter(Q(transaction_type_name=self.type_model.ADJ_DEBIT) | 
                                             Q(transaction_type_name=self.type_model.BANK_FEE) |
                                             Q(transaction_type_name=self.type_model.WITHDRAWAL) |
                                             Q(transaction_type_name=self.type_model.ATM_WITHDRAWAL))
        return super().get_queryset().filter(transaction_type__in=q)


class CreditManager(models.Manager):
    def __init__(self, type_model) -> None:
        self.type_model = type_model
        super().__init__()

    def get_queryset(self):
        q = self.type_model.objects.filter(transaction_type_name=self.type_model.ADJ_CREDIT)
        return super().get_queryset().filter(transaction_type__in=q)


class DebitManager(models.Manager):
    def __init__(self, type_model) -> None:
        self.type_model = type_model
        super().__init__()

    def get_queryset(self):
        q = self.type_model.objects.filter(transaction_type_name=self.type_model.ADJ_DEBIT)
        return super().get_queryset().filter(transaction_type__in=q)
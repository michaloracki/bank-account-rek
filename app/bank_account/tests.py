import datetime
from django.db.models import Sum
from django.test import TestCase
from bank_account.models import *


class StatementTestCase(TestCase):
    def setUp(self):
        TransactionType.objects.create(transaction_type_name=TransactionType.DEPOSIT) 
        TransactionType.objects.create(transaction_type_name=TransactionType.WITHDRAWAL) 
        AccountType.objects.create(
            name=AccountType.CURRENT_ACCOUNT
        )
        self.acc_type = AccountType.objects.get(name=AccountType.CURRENT_ACCOUNT)
        Account.objects.create(
            account_no='TEST1234',
            account_type=self.acc_type,
            first_name='Jan',
            last_name='Kowalski',
            country='PL',
            place_of_birth='Warsaw',
            date_of_birth=datetime.datetime(1970, 1, 1),
            city='Warsaw',
            address='TEST',
            zip_code='00-000',
            phone_number='123456789',
            email='test@test.pl'
        )
        self.date = datetime.datetime.now()
        self.account_no = 'TEST1234'
        self.account = Account.objects.get(account_no=self.account_no)
        self.transaction_type = TransactionType.objects.get(transaction_type_name=TransactionType.DEPOSIT)
        self.transaction_type_2 = TransactionType.objects.get(transaction_type_name=TransactionType.WITHDRAWAL) 

    def test_balance_is_right(self):
        Transaction.objects.create(account_no_id=self.account, transaction_type=self.transaction_type, date=self.date, ammount=100)
        Transaction.objects.create(account_no_id=self.account, transaction_type=self.transaction_type_2, date=self.date, ammount=50)
        income = Transaction.income.filter(account_no=self.account_no).aggregate(Sum('ammount'))['ammount__sum']
        loss = Transaction.loss.filter(account_no=self.account_no).aggregate(Sum('ammount'))['ammount__sum']
        credit = 0
        debit = 0
        
        balance = income - loss
        AccountStatement.objects.create(account_no=self.account, date=self.date, closing_balance=balance, total_credit=credit, total_debit=debit)
        statement = AccountStatement.objects.get(account_no=self.account_no, date=self.date)
        self.assertAlmostEqual(statement.closing_balance, 50.0)

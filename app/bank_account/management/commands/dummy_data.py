import datetime
from django.core.management.base import BaseCommand
from bank_account.models import *


class Command(BaseCommand):
    help = 'Fill database with sample account and all transaction types, the same with account types'

    def handle(self, *args, **options):
        self._create_account_types()
        self._create_transaction_types()
        self._create_account()

    def _create_transaction_types(self):
        for choice in TransactionType.TRANSACTION_CHOICES:
            account_type = TransactionType(transaction_type_name=choice[0])
            account_type.save()

    def _create_account_types(self):
        for choice in AccountType.ACCOUNT_CHOICES:
            account_type = AccountType(name=choice[0])
            account_type.save()

    def _create_account(self):
        account_type = AccountType.objects.filter(name=AccountType.CURRENT_ACCOUNT).first()
        account = Account(
            account_no='49 1020 2892 2276 3005 0000 0000',
            account_type=account_type,
            first_name='Jan',
            last_name='Kowalski',
            country='PL',
            place_of_birth='Warsaw',
            date_of_birth=datetime.datetime(1970, 1, 1),
            city='Warsaw',
            address='TEST',
            zip_code='00-000',
            phone_number='123456789',
            email='test@test.pl'
            )
        account.save()

from django.shortcuts import render, redirect
from django.urls import reverse
from bank_account.models import Transaction, AccountStatement
from bank_account.forms import TransactionForm, StatementForm
from bank_account.utils import StatementGenerator


def show_transactions(request):
    transactions = Transaction.objects.all()
    return render(request, 'transactions.html', context={'transactions': transactions})


def new_transaction(request):
    if request.method == 'POST':
        form = TransactionForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            transaction = Transaction(account_no=data['account_no'], transaction_type=data['transaction_type'],
                                      date=data['date'], ammount=data['ammount'])
            transaction.save()
            return redirect(reverse('transactions'))
    else:
        form = TransactionForm()

    return render(request, 'new_transaction.html', {'form': form})


def show_statements(request):
    statements = AccountStatement.objects.all()
    return render(request, 'statements.html', context={'statements': statements})


def new_statement(request):
    if request.method == 'POST':
        form = StatementForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            StatementGenerator.generate(data)
            return redirect(reverse('statements'))
    else:
        form = StatementForm()

    return render(request, 'new_statement.html', {'form': form})
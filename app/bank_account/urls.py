from hashlib import new
from django.urls import path
from bank_account.views import show_transactions, new_transaction, show_statements, new_statement

urlpatterns = [
    path('show_transactions/', show_transactions, name='transactions'),
    path('new_transaction/', new_transaction, name='new_transaction'),
    path('show_statements/', show_statements, name='statements'),
    path('new_statement/', new_statement, name='new_statement'),
]
from django.db.models import Sum
from bank_account.models import Transaction, AccountStatement


class StatementGenerator:
    @staticmethod
    def generate(data : dict):
        account_no = data['account_no']
        date = data['date']
        income = Transaction.income.filter(account_no=account_no).aggregate(Sum('ammount'))['ammount__sum']
        loss = Transaction.loss.filter(account_no=account_no).aggregate(Sum('ammount'))['ammount__sum']
        credit = Transaction.credit.filter(account_no=account_no).aggregate(Sum('ammount'))['ammount__sum']
        debit = Transaction.debit.filter(account_no=account_no).aggregate(Sum('ammount'))['ammount__sum']

        balance = StatementGenerator.try_get_balance(income, loss)
        credit, debit = StatementGenerator.validate_constraints(credit, debit)
        
        # try:
        statement = AccountStatement(account_no=account_no, date=date, closing_balance=balance, total_credit=credit, total_debit=debit)
        statement.save()
        # except Exception as e:
        #     pass

    @staticmethod
    def try_get_balance(income, loss):
        try:
            balance = income - loss
        except TypeError as e:
            balance = 0
        return balance

    @staticmethod
    def validate_constraints(credit, debit):
        if credit is None:
            credit = 0

        if debit is None:
            debit = 0

        return credit, debit



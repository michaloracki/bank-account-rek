from django.forms import ModelForm, SelectDateWidget
from bank_account.models import Transaction, AccountStatement

class TransactionForm(ModelForm):
    class Meta:
        model = Transaction
        fields = ['account_no', 'transaction_type', 'date', 'ammount']
        widgets = {'date': SelectDateWidget}


class StatementForm(ModelForm):
    class Meta:
        model = AccountStatement
        fields = ['account_no', 'date']
        widgets = {'date': SelectDateWidget}